import requests
# import unittest

base_url = 'http://localhost:3000'

expected_books = [
    {
    "title":"Fantasy",
    "publication information":"Harmonia Mundi",
    "authors":[
        "Chun, Jennifer",
        "Yun, Isang"
    ],
    "copies":1,
    }
]

def read_books():
    books = requests.get(base_url + '/books').json()
    ids = [x['id'] for x in books]
    if len(books) != len(expected_books):
        print('not all books were retrieved')
    for book_id in ids:
        book = requests.get(base_url + '/book/{}'.format(book_id)).json()
        if book['id'] != book_id:
            print('retrieved the wrong book', book['id'], book_id)

def create_books():
    ids = []
    for book in expected_books:
        res = requests.post(base_url + '/book', json=book)
        ids.append(res.json()['id'])

    for i in range(len(expected_books)):
        book = expected_books[i]
        expected_book = requests.get(base_url + '/book/{}'.format(ids[i])).json()
        for val in book:
            if val not in expected_book:
                print('created book does not contain value', val)
                continue
            if book[val] != expected_book[val]:
                print('book field did not match after create', val, book[val],  expected_book[val])
        if 'id' not in expected_book:
            print('id not created')
        if expected_book['holds'] != 0:
            print('holds has incorrect value after create')

def update_books():
    books = requests.get(base_url + '/books').json()
    update_fields = [('title', 'Wrong Title'), ('authors', ['fake author'])]
    for book in books:
        for (key, val) in update_fields:
            book[key] = val
        book_url = base_url + '/book/{}'.format(book['id'])
        res = requests.put(book_url, json=book)
        if not res.ok:
            print('failure updating book', book['id'])
        updated_book = requests.get(book_url).json()
        for (key, val) in update_fields:
            if updated_book[key] != val:
                print('field not updated {} != {}'.format(updated_book[key], val))

def delete_books():
    books = requests.get(base_url + '/books').json()
    for book in books:
        res = requests.delete(base_url + '/book/{}'.format(book['id']))
        if not res.ok:
            print('delete book', book['id'], res.status_code, res.text)

def verify_clean_state():
    books = requests.get(base_url + '/books').json()
    if books != []:
        print('all books were not deleted', books)


if __name__ == "__main__":
    create_books()
    read_books()
    update_books()
    delete_books()
    verify_clean_state()
