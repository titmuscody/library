FROM golang:1.14.7-alpine3.12 as builder

WORKDIR /go/src/digicert.com/library
COPY go.mod go.mod
COPY main.go main.go
COPY api api
COPY db db
COPY models models

RUN go build

FROM alpine:latest
WORKDIR /root/
COPY --from=builder /go/src/digicert.com/library/library .
CMD ["./library"]
