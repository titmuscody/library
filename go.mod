module digicert.com/library

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/gocql/gocql v0.0.0-20200815110948-5378c8f664e9
	github.com/google/uuid v1.1.1
	github.com/stretchr/testify v1.4.0
)
