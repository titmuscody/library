package api

import (
	"log"
	"net/http"

	"digicert.com/library/db"
	"digicert.com/library/models"
	"github.com/gin-gonic/gin"
)

var bookDB db.BookDB

func init() {
	var err error
	bookDB, err = db.New()
	if err != nil {
		log.Fatalf("could not initialize database connection: %s", err)
	}
}

// CreateBook uses gin context to create a library book
// json body is used to the fields
// a book cannot specify the id or holds value
// on success we return the in the form {"id": "uuid"}
func CreateBook(c *gin.Context) {
	var book models.Book
	if err := c.BindJSON(&book); err != nil {
		c.JSON(http.StatusBadRequest, err)
	}
	if book.Holds != 0 {
		c.JSON(http.StatusBadRequest, "holds cannot be set through api")
	}
	if book.ID != "" {
		c.JSON(http.StatusBadRequest, "the book id cannot be set on creation")
	}
	id, err := bookDB.SaveBook(book)
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
	}
	c.JSON(http.StatusOK, gin.H{"id": id})
}

// GetAllBooks retrieves all books in database and returns as a list
func GetAllBooks(c *gin.Context) {
	books, err := bookDB.GetAllBooks()
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
	}
	c.JSON(http.StatusOK, books)
}

// GetBook retrieves the book with the id found in the url
func GetBook(c *gin.Context) {
	bookID := c.Param("id")
	book, err := bookDB.LoadBook(bookID)
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
	}
	c.JSON(http.StatusOK, book)
}

// DeleteBook will remove the book with the id from the database
func DeleteBook(c *gin.Context) {
	bookID := c.Param("id")
	bookDB.DeleteBook(bookID)
	c.JSON(http.StatusOK, "")
}

// UpdateBook updates the book specified in the url id
// Holds and ID cannot be updated by a user
func UpdateBook(c *gin.Context) {
	bookID := c.Param("id")
	var book models.Book
	if err := c.BindJSON(&book); err != nil {
		c.JSON(http.StatusBadRequest, err)
	}
	if book.Holds != 0 {
		c.JSON(http.StatusBadRequest, "holds cannot be set through api")
	}
	if book.ID != bookID {
		c.JSON(http.StatusBadRequest, "the book id cannot be updated")
	}
	bookDB.UpdateBook(book)
	c.JSON(http.StatusOK, "")
}

// Status returns the status of the server
// currently there is no way for the server to enter a bad state so we always return ok
func Status(c *gin.Context) {
	c.JSON(http.StatusOK, "ok")
}
