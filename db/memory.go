package db

import (
	"fmt"
	"sync"

	"digicert.com/library/models"
	"github.com/google/uuid"
)

// MemoryDB implements an in memory db for storing books
type MemoryDB struct {
	mem sync.Map
}

// SaveBook saves a book to the in memory database
func (db *MemoryDB) SaveBook(book models.Book) (string, error) {
	book.ID = uuid.New().String()
	err := db.UpdateBook(book)
	return book.ID, err
}

// UpdateBook updates book in the in memory database
func (db *MemoryDB) UpdateBook(book models.Book) error {
	db.mem.Store(book.ID, book)
	return nil
}

// LoadBook retrieves book from the in memory database
func (db *MemoryDB) LoadBook(id string) (models.Book, error) {
	book, ok := db.mem.Load(id)
	if !ok {
		return models.Book{}, fmt.Errorf("book not found in db")
	}
	return book.(models.Book), nil
}

// DeleteBook removes book from in memeory database
func (db *MemoryDB) DeleteBook(id string) error {
	db.mem.Delete(id)
	return nil
}

// GetAllBooks retrieves all books from in memeory database
func (db *MemoryDB) GetAllBooks() ([]models.Book, error) {
	books := []models.Book{}
	updateBooks := func(key, val interface{}) bool {
		books = append(books, val.(models.Book))
		return true
	}
	db.mem.Range(updateBooks)
	return books, nil
}
