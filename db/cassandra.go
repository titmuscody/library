package db

import (
	"digicert.com/library/models"
	"github.com/gocql/gocql"
)

// CassandraDB implements the db with Cassandra for the backend
type CassandraDB struct {
	session *gocql.Session
}

// SaveBook saves a book to the in memory database
func (db *CassandraDB) SaveBook(book models.Book) (string, error) {
	uuid := gocql.TimeUUID()
	book.ID = uuid.String()

	err := db.UpdateBook(book)
	return uuid.String(), err
}

// UpdateBook updates book in the in memory database
func (db *CassandraDB) UpdateBook(book models.Book) error {
	uuid, err := gocql.ParseUUID(book.ID)
	if err != nil {
		return err
	}
	// due to the way cassandra updates entries inserting will update existing values
	err = db.session.
		Query(`INSERT INTO books (id, title, publication, authors, holds, copies) values(?, ?, ?, ?, ?, ?)`,
			uuid, book.Title, book.PublicationInformation, book.Authors, book.Holds, book.Copies).
		Exec()
	return nil
}

// LoadBook retrieves book from the in memory database
func (db *CassandraDB) LoadBook(id string) (models.Book, error) {
	var book models.Book
	var uuid gocql.UUID
	err := db.session.
		Query(`SELECT id, title, publication, authors, holds, copies FROM books WHERE id = ? LIMIT 1`, id).
		Consistency(gocql.One).
		Scan(&uuid, &book.Title, &book.PublicationInformation, &book.Authors, &book.Holds, &book.Copies)
	book.ID = uuid.String()
	return book, err
}

// DeleteBook removes book from in memeory database
func (db *CassandraDB) DeleteBook(id string) error {
	uuid, err := gocql.ParseUUID(id)
	if err != nil {
		return err
	}
	err = db.session.
		Query(`DELETE FROM books WHERE id = ?`, uuid).
		Exec()
	return err
}

// GetAllBooks retrieves all books from in memeory database
func (db *CassandraDB) GetAllBooks() ([]models.Book, error) {
	books := []models.Book{}
	var book models.Book
	var uuid gocql.UUID
	iter := db.session.Query(`SELECT id, title, publication, authors, holds, copies FROM books`).Iter()
	for iter.Scan(&uuid, &book.Title, &book.PublicationInformation, &book.Authors, &book.Holds, &book.Copies) {
		book.ID = uuid.String()
		books = append(books, book)
	}
	return books, nil
}
