package db_test

import (
	"testing"

	"digicert.com/library/db"
	"digicert.com/library/models"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestBook(t *testing.T) {
	bookDB, err := db.New()
	require.NoError(t, err, "could not initialize db")
	book := models.Book{
		Title:                  "test book",
		PublicationInformation: "info",
		Authors:                []string{"author1", "author2"},
		Holds:                  0,
		Copies:                 1,
		ID:                     "",
	}
	id, err := bookDB.SaveBook(book)
	assert.NotEqual(t, id, "", "id cannot be a empty string")
	require.NoError(t, err, "error saving book")

	book.ID = id
	loadedBook, err := bookDB.LoadBook(id)
	require.NoError(t, err, "error loading book")
	assert.Equal(t, loadedBook, book, "retrieved book should be equal")

	book.Title = "new title"
	err = bookDB.UpdateBook(book)
	assert.NoError(t, err, "error updating book")

	updatedBook, err := bookDB.LoadBook(book.ID)
	assert.NoError(t, err, "error loading updated book")
	assert.Equal(t, updatedBook.Title, "new title")

	err = bookDB.DeleteBook(book.ID)
	assert.NoError(t, err, "error deleting book")

	_, err = bookDB.LoadBook(book.ID)
	assert.Error(t, err, "retrieving deleted book should of failed")

}

func TestGetAllBooks(t *testing.T) {
	bookDB, err := db.New()
	require.NoError(t, err, "could not initialize db")
	book := models.Book{
		Title:                  "test book",
		PublicationInformation: "info",
		Authors:                []string{"author1", "author2"},
		Holds:                  0,
		Copies:                 1,
		ID:                     "",
	}
	books, err := bookDB.GetAllBooks()
	require.NoError(t, err, "could not get books")
	require.Equal(t, len(books), 0, "cannot run test if books in db")
	for i := 0; i < 10; i++ {
		_, err := bookDB.SaveBook(book)
		assert.NoError(t, err, "error saving book")
	}

	books, err = bookDB.GetAllBooks()
	assert.NoError(t, err, "error getting all books")
	assert.Equal(t, len(books), 10, "did not retrieve all inserted book:")

}

func BenchmarkCreateBook(b *testing.B) {
	bookDB, err := db.New()
	if err != nil {
		b.FailNow()
	}
	book := models.Book{
		Title:                  "test book",
		PublicationInformation: "info",
		Authors:                []string{"author1", "author2"},
		Holds:                  0,
		Copies:                 1,
		ID:                     "",
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		bookDB.SaveBook(book)
	}
}
