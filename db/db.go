package db

import (
	"os"
	"strings"
	"sync"

	"digicert.com/library/models"
	"github.com/gocql/gocql"
)

// BookDB database interface to allow multiple backends
type BookDB interface {
	SaveBook(models.Book) (string, error)
	UpdateBook(models.Book) error
	LoadBook(string) (models.Book, error)
	DeleteBook(string) error
	GetAllBooks() ([]models.Book, error)
}

// New handles the selection and creation of BookDB
func New() (BookDB, error) {

	seedString, ok := os.LookupEnv("CASSANDRA_SEEDS")
	if !ok {
		return &MemoryDB{mem: sync.Map{}}, nil
	}
	keyspace, ok := os.LookupEnv("CASSANDRA_KEYSPACE")
	if !ok {
		keyspace = "library"
	}

	// return &MemoryDB{mem: sync.Map{}}, nil
	seeds := strings.Split(seedString, ",")
	cluster := gocql.NewCluster(seeds...)
	cluster.Keyspace = keyspace
	cluster.Consistency = gocql.Quorum
	session, err := cluster.CreateSession()

	db := &CassandraDB{session: session}
	return db, err
}
