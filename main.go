package main

import (
	"os"

	"digicert.com/library/api"
	"github.com/gin-gonic/gin"
)

func main() {
	port, ok := os.LookupEnv("PORT")
	if !ok {
		port = "3000"
	}
	router := gin.Default()
	router.GET("/status", api.Status)

	router.GET("/books", api.GetAllBooks)

	router.DELETE("/book/:id", api.DeleteBook)
	router.PUT("/book/:id", api.UpdateBook)
	router.GET("/book/:id", api.GetBook)

	router.POST("/book", api.CreateBook)

	router.Run(":" + port)
}
