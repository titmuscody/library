package models

// Book stores the information for a library book
// Holds is a read only field reserved for a future release
type Book struct {
	Title                  string   `json:"title" binding:"required"`
	PublicationInformation string   `json:"publication information"`
	Authors                []string `json:"authors"`
	Holds                  int64    `json:"holds"`
	Copies                 int64    `json:"copies" binding:"required"`
	ID                     string   `json:"id"`
}
