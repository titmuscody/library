# Library

library application for managing books in a library

# Building
## locally
```bash
go build main.go
```
## building docker image
```bash
docker build . --tag library
```
# Running
## env
* PORT - which port will be used for the web server. default to 3000
* CASSANDRA_SEEDS - comman separated list of urls for cassandra servers. If not specified in memory DB is used
* CASSANDRA_KEYSPACE - keyspace the cassandra client should connect to. defaults to library
## locally
```bash
go run main.go
```
## docker
```bash
docker run --rm -p 3000:3000 --name library library
```
# tests
## unit tests
```bash
go test digicert.com/library/db
```
## benchmark tests
```bash
go test -bench=. digicert.com/library/db
```
## end to end
```bash
pip3 install -r requirements.txt
python3 test.py
```


# Database Setup
## Cassandra
### install local instance
```bash
docker run --name cassandra --network host -d cassandra:latest
```
### create schema
```cqlsh
CREATE KEYSPACE library WITH replication = {'class':'SimpleStrategy', 'replication_factor': 1};
CREATE TABLE books(
    id UUID PRIMARY KEY,
    title text,
    publication text,
    authors list<text>,
    holds int,
    copies int,
);
```